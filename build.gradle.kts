import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.0"
}

group = "me.profiluefter"
version = "1.0"

repositories {
    jcenter()
}

dependencies {
    implementation("net.dv8tion", "JDA", "4.2.0_200")
    implementation("com.sun.mail", "javax.mail", "1.6.2")
    implementation("org.slf4j", "slf4j-simple", "1.7.30")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}