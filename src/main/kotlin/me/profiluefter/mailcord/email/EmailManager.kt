package me.profiluefter.mailcord.email

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

interface EmailManager : EmailSender, EmailSynchronizer {
    companion object {
        private val managers: MutableMap<Long, EmailManager> = mutableMapOf()
        fun getByGuildID(id: Long): EmailManager = managers.computeIfAbsent(id, ::createForGuild)

        private fun createForGuild(id: Long): EmailManager {
            //TODO: decide which implementation to use based on the planed #settings channel
            return CompositeEmailManager(
                ConsoleLogSender(id),
                IMAPSynchronizer(id)
            )
        }

        private val executor: ScheduledExecutorService = Executors.newScheduledThreadPool(2)
    }

    fun startSynchronization() {
        executor.scheduleWithFixedDelay({ synchronize() }, 5, 30, TimeUnit.SECONDS)
    }
}

interface EmailSender {
    fun sendEmail(recipient: String, subject: String, content: String)
}

interface EmailSynchronizer {
    fun synchronize()
}

class CompositeEmailManager(private val sender: EmailSender, private val synchronizer: EmailSynchronizer) :
    EmailManager {
    override fun sendEmail(recipient: String, subject: String, content: String) =
        sender.sendEmail(recipient, subject, content)

    override fun synchronize() =
        synchronizer.synchronize()
}