package me.profiluefter.mailcord.email

import com.sun.mail.imap.IdleManager
import me.profiluefter.mailcord.discord.SettingsManager
import me.profiluefter.mailcord.jda
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import java.util.*
import java.util.concurrent.Executors
import javax.mail.*
import javax.mail.internet.InternetAddress

class IMAPSynchronizer(private val guildID: Long) : EmailSynchronizer {
    private val store by lazy {
        try {
            initJavaMail()
        } catch (e: Exception) {
            e.printStackTrace(); throw Exception()
        }
    }
    private val folderDiscordMapping: Map<Folder, TextChannel> by lazy {
        try {
            initFolderSynchronization()
        } catch (e: Exception) {
            e.printStackTrace(); throw Exception()
        }
    }

    private val settingsManager = SettingsManager.getByGuildID(guildID)
    private lateinit var idleManager: IdleManager
    private var initialSync: Boolean = true

    private fun initJavaMail(): Store {
        val properties = Properties()
        val host = settingsManager["imap.host"]
        val port = settingsManager["imap.port"]
        val user = settingsManager["imap.user"]
        val pass = settingsManager["imap.pass"]
        properties["mail.imap.host"] = host
        properties["mail.imap.port"] = port
        properties["mail.imap.user"] = user

        properties["mail.imaps.usesocketchannels"] = true //Needed for IdleManager

        val session = Session.getInstance(properties)
        session.debug = false
        idleManager = IdleManager(session, Executors.newCachedThreadPool())
        val store = session.getStore("imaps")
        store.connect(host, user, pass)
        return store
    }

    private fun initFolderSynchronization(): Map<Folder, TextChannel> {
        @Suppress("SpellCheckingInspection")
        System.err.println("ABFAHRT GEMMA JAVAMAIL JAWOI")

        var rootFolder = store.getFolder("INBOX")
        while (rootFolder.parent != null)
            rootFolder = rootFolder.parent

        val allFolders = rootFolder.list("*")
        allFolders.forEach {
            try {
                it.open(Folder.READ_ONLY)
            } catch (e: Exception) {
                return@forEach
            }
        }
        val folders = allFolders
            .filter {
                try {
                    it.mode == Folder.HOLDS_MESSAGES
                } catch (e: Exception) {
                    return@filter false
                }
            }

        folders.forEach(idleManager::watch)

        val guild = jda.getGuildById(guildID) ?: throw IllegalStateException()
        val username = settingsManager["imap.user"]
        val category =
            guild.getCategoriesByName(username, true).getOrNull(0)
                ?: guild.createCategory(username).complete()

        return folders.associateWith { folder ->
            val cleanedName = folder.name.toLowerCase().replace(" ", "-")
            category.textChannels
                .find { it.name.equals(cleanedName, true) }
                ?: category.createTextChannel(cleanedName).complete()
        }
    }

    override fun synchronize() {
        if (initialSync) {
            initialSync = false

            folderDiscordMapping.entries.forEach { (folder, channel) ->
                val iterator = channel.iterableHistory.reversed().listIterator()

                folder.messages.forEach { message ->
                    if (!iterator.hasNext()) {
                        channel.sendMessage(message.toEmbed()).queue()
                    } else {
                        val discordMessage = iterator.next()
                        val embed = discordMessage.embeds[0]

                        if (message.messageNumber != embed.footer?.text?.removePrefix("Message Nr. ")?.toInt()) {
                            System.err.println("Discrepancy detected! Message Nr. ${message.messageNumber} != ${embed.footer?.text}")
                        }

                        if (message.toEmbed() != embed) {
                            discordMessage.editMessage(message.toEmbed()).queue()
                        }
                    }
                }
            }

            System.err.println("Finished initial sync")
        } else {
            System.err.println("TODO: partial sync")
        }
    }
}

fun Address.formatString(): String {
    assert(this is InternetAddress)
    val address = this as InternetAddress
    return "${address.personal}<${address.address}>"
}

fun Message.toEmbed(): MessageEmbed = EmbedBuilder()
    .setTitle(subject)
    .setDescription(content.toString())
    .setAuthor(from.joinToString(transform = Address::formatString))
    .setTimestamp(sentDate.toInstant())
    .setFooter("Message Nr. $messageNumber")
    .build()