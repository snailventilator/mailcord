package me.profiluefter.mailcord.email

class ConsoleLogSender(private val guildID: Long) : EmailSender {
    override fun sendEmail(recipient: String, subject: String, content: String) {
        println("Server $guildID sending email to $recipient: $subject\n$content")
    }
}
