package me.profiluefter.mailcord.discord

import me.profiluefter.mailcord.jda
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent
import net.dv8tion.jda.api.requests.RestAction
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

class SettingsManager private constructor(private val guildID: Long) {
    companion object {
        private const val editEmoji = "U+270fU+fe0f"
        private const val errorEmoji = "U+1F6AB"

        private val managers: MutableMap<Long, SettingsManager> = mutableMapOf()

        fun getByGuildID(id: Long) =
            managers.computeIfAbsent(id) { SettingsManager(id) }

        val settings: Properties by lazy {
            Properties().apply {
                load(SettingsManager::class.java.getResourceAsStream("settings.properties"))
            }
        }
    }

    operator fun get(propertyName: String): String {
        val name = settings[propertyName] ?: throw IllegalArgumentException("Unknown property $propertyName")

        val guild = jda.getGuildById(guildID)
        val category = guild?.getCategoriesByName("Settings", false)?.getOrNull(0)
        val channel = category?.textChannels?.find { it.name == propertyName.split(".")[0] }
        val message = channel?.iterableHistory?.find { it.contentRaw.startsWith("**$name**: ") }

        return message?.contentRaw?.removePrefix("**$name**: ")
            ?: throw IllegalStateException("Unable to find $propertyName for guild $guildID")
    }

    fun prepareGuildChannels(guild: Guild) {
        val existingCategory = guild.getCategoriesByName("Settings", false).getOrNull(0)
        if (existingCategory == null) {
            guild.createCategory("Settings").queue { category ->
                settings.keys
                    .map { it as String }
                    .filter { !it.endsWith(".default") }
                    .groupBy { it.split(".")[0] }
                    .forEach { (channelName, properties) ->
                        category.createTextChannel(channelName).queue { channel ->
                            properties.forEach { property ->
                                val name = settings[property]
                                val defaultValue = settings["$property.default"]
                                channel.sendMessage("**$name**: $defaultValue").queue {
                                    it.addReaction(editEmoji).queue() //Add pencil emote
                                }
                            }
                        }
                    }
            }
        } else {
            val channelNames = settings.keys
                .map { (it as String).split(".")[0] }
                .distinct()
            val existingChannels = existingCategory.textChannels

            val restActions = channelNames.filter { name ->
                existingChannels.none {
                    it.name == name
                }
            }.map {
                existingCategory.createTextChannel(it) //No queue needed
            }

            fun populateChannels(createdChannels: List<TextChannel> = emptyList()) {
                (createdChannels + existingChannels).forEach { channel ->
                    val history = channel.history
                    //This assumes that there are less than 100 settings/channel. Very limiting
                    history.retrievePast(100).queue {
                        settings.filterKeys {
                            (it as String).startsWith(channel.name) && !it.endsWith(".default")
                        }.forEach { entry ->
                            val property = entry.key as String
                            val name = entry.value as String

                            if (
                                history.retrievedHistory.none {
                                    it.contentRaw.startsWith("**$name**: ")
                                }
                            ) {
                                val defaultValue = settings["$property.default"]
                                channel.sendMessage("**$name**: $defaultValue").queue {
                                    it.addReaction(editEmoji).queue() //Add pencil emote
                                }
                            }
                        }
                    }
                }
            }

            if (restActions.isNotEmpty())
                RestAction.allOf(restActions).queue(::populateChannels)
            else
                populateChannels()
        }
    }

    fun onReactionAdd(event: GuildMessageReactionAddEvent) {
        if (event.reactionEmote.asCodepoints != editEmoji)
            return event.reaction.clearReactions().queue()
    }

    fun onReactionRemove(event: GuildMessageReactionRemoveEvent) {

    }

    fun onMessageSent(event: GuildMessageReceivedEvent) {
        val message = event.channel
            .iterableHistory
            .filter { it.author == event.jda.selfUser }
            .find { it.reactions.getOrNull(0)?.count ?: 0 > 1 }
        if (message == null) {
            event.message.addReaction(errorEmoji).queue()
            event.message.delete().queueAfter(10, TimeUnit.SECONDS)
            return
        }

        val matcher = Pattern.compile("\\*\\*(.*?)\\*\\*: .*").matcher(message.contentRaw)
        if (!matcher.matches())
            throw IllegalStateException("Unknown message in settings channel: ${message.id} ${message.contentRaw}")
        val name = matcher.group(1)

        message.editMessage("**$name**: ${event.message.contentRaw}").queue {
            event.message.delete().queue()
        }
        message.clearReactions().queue {
            message.addReaction(editEmoji).queue()
        }
    }
}