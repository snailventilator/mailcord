package me.profiluefter.mailcord.discord

import me.profiluefter.mailcord.email.EmailManager
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.GuildReadyEvent
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class DiscordManager(guildID: Long) {
    companion object : ListenerAdapter() {
        private val managers: MutableMap<Long, DiscordManager> = mutableMapOf()

        private fun getByGuildID(id: Long): DiscordManager =
            managers.computeIfAbsent(id) { DiscordManager(id) }

        override fun onGuildMessageReceived(event: GuildMessageReceivedEvent) =
            getByGuildID(event.guild.idLong).onGuildMessageReceived(event)

        override fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) =
            getByGuildID(event.guild.idLong).onGuildMessageReactionAdd(event)

        override fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) =
            getByGuildID(event.guild.idLong).onGuildMessageReactionRemove(event)

        override fun onGuildReady(event: GuildReadyEvent) =
            getByGuildID(event.guild.idLong).onGuildReady(event)

        override fun onGuildJoin(event: GuildJoinEvent) =
            getByGuildID(event.guild.idLong).onGuildJoin(event)
    }

    private val draftManager = DraftManager.getByGuildID(guildID)
    private val settingsManager = SettingsManager.getByGuildID(guildID)

    fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        if (event.user == event.jda.selfUser) return
        if (event.channel == draftManager.draftChannel) return draftManager.onReactionAdd(event)
        if (event.channel.parent?.name == "Settings") return settingsManager.onReactionAdd(event)
    }

    fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) {
        if (event.user == event.jda.selfUser) return
        if (event.channel == draftManager.draftChannel) return draftManager.onReactionRemove(event)
        if (event.channel.parent?.name == "Settings") return settingsManager.onReactionRemove(event)
    }

    fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        if (event.author.isBot) return
        if (event.channel == draftManager.draftChannel) return draftManager.onMessageSent(event)
        if (event.channel.parent?.name == "Settings") return settingsManager.onMessageSent(event)
    }

    private fun onGuildReady(event: GuildReadyEvent) = prepareGuildChannels(event.guild)
    private fun onGuildJoin(event: GuildJoinEvent) = prepareGuildChannels(event.guild)

    private fun prepareGuildChannels(guild: Guild) {
        SettingsManager.getByGuildID(guild.idLong).prepareGuildChannels(guild)
        DraftManager.getByGuildID(guild.idLong).prepareGuildChannels(guild)
        EmailManager.getByGuildID(guild.idLong).startSynchronization()
    }
}
