package me.profiluefter.mailcord.discord

import me.profiluefter.mailcord.email.EmailManager
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent

//TODO: Draft deletion via cancel reaction
class DraftManager private constructor(private val guildID: Long) {
    companion object {
        private val managers: MutableMap<Long, DraftManager> = mutableMapOf()

        fun getByGuildID(id: Long) =
            managers.computeIfAbsent(id) { DraftManager(id) }
    }

    lateinit var draftChannel: TextChannel
    private val settingsManager = SettingsManager.getByGuildID(guildID)
    private val messageCache: MutableCollection<Long> = mutableSetOf()

    private var currentlyEditingMessage: Message? = null
    private var currentlyEditingEmoji: Emoji? = null
    private var currentlyEditingAnnouncement: Message? = null

    fun prepareGuildChannels(guild: Guild) {
        val channel = guild.textChannels
            .filter { it.parent == null }
            .find { it.name == "drafts" }
        if (channel == null)
            guild
                .createTextChannel("drafts")
                .setTopic("Mention MailCord in here to create a new draft!")
                .queue { draftChannel = it }
        else
            draftChannel = channel
    }

    fun onMessageSent(event: GuildMessageReceivedEvent) {
        if (event.message.mentionedUsers.contains(event.jda.selfUser)) {
            //TODO: remove reaction and delete announcement
            currentlyEditingMessage = null
            currentlyEditingEmoji = null
            currentlyEditingAnnouncement = null
            sendNewEmbed(event.channel)
            event.message.delete().queue()
            return
        }

        if (currentlyEditingMessage != null) {
            var (to, subject, content) = currentlyEditingMessage!!.embeds[0].fields.map { it.value!! }
            when (currentlyEditingEmoji) {
                Emoji.To -> to = event.message.contentDisplay
                Emoji.Subject -> subject = event.message.contentDisplay
                Emoji.Content -> content = event.message.contentDisplay
                else -> throw IllegalStateException()
            }
            currentlyEditingMessage!!.editMessage(createMessageEmbed(to, subject, content)).queue()
            currentlyEditingMessage!!.removeReaction(currentlyEditingEmoji!!.unicode, event.author).queue()
            currentlyEditingAnnouncement!!.delete().queue()
            event.message.delete().queue()
            currentlyEditingMessage = null
            currentlyEditingEmoji = null
            currentlyEditingAnnouncement = null
        }
    }

    fun onReactionAdd(event: GuildMessageReactionAddEvent) {
        //TODO: Avoid duplicate code
        val inCache = messageCache.contains(event.messageIdLong)
        val message: Message by lazy {
            event.retrieveMessage().complete()
        }

        if (!inCache && (message.author != event.jda.selfUser || message.embeds.size != 1)) {
            return
        }

        when (val emoji = Emoji.fromString(event.reactionEmote.asReactionCode)) {
            Emoji.Send -> {
                val (to, subject, content) = message.embeds[0].fields.map { it.value!! } //Kotlin == sick
                val saveError: (t: Message) -> Unit = {
                    currentlyEditingMessage = message
                    currentlyEditingEmoji = Emoji.Send
                    currentlyEditingAnnouncement = it
                }
                if (to == "*Empty*" || to.isEmpty()) {
                    message.channel.sendMessage("Invalid email address `${to}`").queue(saveError)
                    return
                }
                if (subject == "*Empty*" || subject.isEmpty()) {
                    message.channel.sendMessage("Empty subject!").queue(saveError)
                    return
                }
                if (content == "*Empty*" || content.isEmpty()) {
                    message.channel.sendMessage("Empty content!").queue(saveError)
                    return
                }
                EmailManager.getByGuildID(event.guild.idLong).sendEmail(to, subject, content)
            }
            null -> event.reaction.clearReactions().queue()
            else -> {
                currentlyEditingMessage = message
                currentlyEditingEmoji = emoji
                currentlyEditingAnnouncement = message.channel.sendMessage("Please enter the new $emoji:").complete()
            }
        }
    }

    fun onReactionRemove(event: GuildMessageReactionRemoveEvent) {
        //TODO: Avoid duplicate code
        val inCache = messageCache.contains(event.messageIdLong)
        val message: Message by lazy {
            event.retrieveMessage().complete()
        }

        if (!inCache && (message.author != event.jda.selfUser || message.embeds.size != 1)) {
            return
        }

        val emoji = Emoji.fromString(event.reactionEmote.asReactionCode) ?: return
        if (emoji == currentlyEditingEmoji) {
            currentlyEditingAnnouncement!!.delete().queue()
            currentlyEditingMessage = null
            currentlyEditingEmoji = null
            currentlyEditingAnnouncement = null
        }
    }

    private fun sendNewEmbed(channel: TextChannel) {
        channel.sendMessage(createMessageEmbed()).queue { message ->
            messageCache.add(message.idLong)
            Emoji.values().forEach {
                message.addReaction(it.unicode).queue()
            }
        }
    }

    private fun createMessageEmbed(
        to: String = settingsManager["email.default.to"],
        subject: String = settingsManager["email.default.subject"],
        content: String = settingsManager["email.default.content"]
    ): MessageEmbed = EmbedBuilder().apply {
        addField(":one: To", to, false)
        addField(":two: Subject", subject, false)
        addField(":three: Content", content, false)
    }.build()
}