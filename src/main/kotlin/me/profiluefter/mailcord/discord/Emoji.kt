package me.profiluefter.mailcord.discord

enum class Emoji(val unicode: String) {
    //TODO: Replace this with escape sequences or unicode notation if JDA supports that
    To("1️⃣"), Subject("2️⃣"), Content("3️⃣"), Send("\uD83D\uDD25");

    companion object {
        fun fromString(unicode: String): Emoji? {
            return values().find {
                it.unicode == unicode
            }
        }
    }
}