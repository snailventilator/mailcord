package me.profiluefter.mailcord

import me.profiluefter.mailcord.discord.DiscordManager
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder

lateinit var jda: JDA

fun main() {
    jda = JDABuilder
        .createDefault(System.getenv("TOKEN"))
        .addEventListeners(DiscordManager)
        .build()
        .awaitReady()
}